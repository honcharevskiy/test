from django.contrib import admin
from articles.models import Page, PageCopy

# Register your models here.
admin.site.register(Page)
admin.site.register(PageCopy)
