from django.db import models

# Create your models here.


class Page(models.Model):
    title = models.CharField(max_length=150)
    text = models.TextField()
    production_ver = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.production_ver:
            try:
                production_page = PageCopy.objects.get(page=self.id, production_ver=True)
                production_page.production_ver = False
                production_page.save()
            except:
                pass
        super(Page, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class PageCopy(models.Model):
    title = models.CharField(max_length=150)
    text = models.TextField()
    production_ver = models.BooleanField(default=False)
    page = models.ForeignKey(Page, related_name='page', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if self.production_ver:
            if not self.page.production_ver:
                try:
                    production_page = PageCopy.objects.get(page=self.page, production_ver=True)
                    production_page.production_ver = False
                    production_page.save()
                except:
                    pass
            else:
                production_page = Page.objects.get(pk=self.page.id)
                production_page.production_ver = False
                production_page.save()
        super(PageCopy, self).save(*args, **kwargs)

    def __str__(self):
        return 'Version of "{0}" with id {1}'.format(self.page.title, self.id)
