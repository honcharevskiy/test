from articles.models import Page, PageCopy
from rest_framework import serializers


class PageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Page
        fields = ('id', 'title', 'text', 'production_ver')


class PageCopySerializer(serializers.ModelSerializer):
    class Meta:
        model = PageCopy
        fields = ('id', 'title', 'text', 'production_ver', 'page')
