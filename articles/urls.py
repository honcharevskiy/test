from django.conf.urls import url
from articles import views

urlpatterns = [
    url('pages/$', views.pages_list),
    url('pages/(?P<pk>[0-9]+)/$', views.list_of_page_versions),
    url('pages/(?P<page_id>[0-9]+)/(?P<pk>[0-9]+)/$', views.PageDetail.as_view()),

]
