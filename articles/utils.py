from articles.models import Page, PageCopy

from articles.serializers import PageSerializer, PageCopySerializer


def get_production_version(page_id):
    main_page = Page.objects.get(pk=page_id)
    if main_page.production_ver:
        return PageSerializer(main_page, many=True)
    production_version = PageCopy.objects.filter(page=main_page, production_ver=True)
    return PageCopySerializer(production_version, many=True)


def update_production_version(page_id, copy_page_id=False):
    if not copy_page_id:
        production_version = Page.objects.get(pk=page_id)
        production_version.production_ver = True
        production_version.save()
    else:
        production_version = PageCopy.objects.get(page_id=page_id, pk=copy_page_id)
        production_version.production_ver = True
        production_version.save()
