from itertools import chain

from django.shortcuts import get_object_or_404

from articles.models import Page, PageCopy
from articles.serializers import PageSerializer, PageCopySerializer

from rest_framework import status, generics, permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
# Create your views here.
from articles.utils import get_production_version, update_production_version


@api_view(['GET', 'POST'])
def pages_list(request):
    if request.method == 'GET':
        pages_queryset = PageSerializer(Page.objects.all(), many=True)
        pages_copy_queryset = PageCopySerializer(PageCopy.objects.all(), many=True)
        result_queryset = list(chain(pages_queryset.data, pages_copy_queryset.data))
        return Response(result_queryset)

    if request.method == "POST":
        serializer = PageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def list_of_page_versions(request, pk):
    page = get_object_or_404(Page, id=pk)

    if request.method == 'GET':
        if request.GET.get('production'):
            production_page = get_production_version(page.id)
            return Response(production_page.data)
        elif request.GET.get('make_production'):
            update_production_version(page_id=page.pk)
        filtered_pages = PageCopy.objects.filter(page_id=page.id)
        serializer = PageCopySerializer(filtered_pages, many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PageSerializer(page, data=request.data)
        if serializer.is_valid():
            page_copy = PageCopy(title=request.data['title'],
                                 text=request.data['text'],
                                 page=page,
                                 production_ver=True)
            page_copy.save()
            return Response(PageCopySerializer(page_copy, many=True))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PageDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PageCopySerializer

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = [permissions.AllowAny, ]
        else:
            self.permission_classes = [permissions.IsAdminUser, ]
        return super(PageDetail, self).get_permissions()

    def get_queryset(self):
        if self.request.query_params.get('make_production'):
            get_object_or_404(PageCopy, pk=self.kwargs['pk'])
            update_production_version(page_id=self.kwargs['page_id'], copy_page_id=self.kwargs['pk'])
        return PageCopy.objects.all()
