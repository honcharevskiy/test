-GET api/pages/
response: [{
        "id": <id>,
        "title": "<title>",
        "text": "<text>",
        "production_ver": <true/false>
    }, ...]

-POST api/pages/
data: {"title": "<title>",
        "text": "<text>",
        ["production_ver": <true/false> default=True]}
response: [{
        "id": <created_page_id>,
        "title": "<created_page_title>",
        "text": "<created_page_text>",
        "production_ver": <true/false>
    }]

-GET api/pages/<pk>/
response: [{
        "id": <id>,
        "title": "<title>",
        "text": "<text>",
        "production_ver": <true/false>,
        "page": "<main_page_id>"
    }, ...]

-PUT api/pages/<pk>/
data: {"title": "<title>",
        "text": "<text>",
        ["production_ver": <true/false> default=False]}
response: [{
        "id": <created_copy_page_id>,
        "title": "<created_copy_page_title>",
        "text": "<created_copy_page_text>",
        "production_ver": <true/false>
    }]

-DELETE api/pages/<pk>/
response: []


-GET pages/<page_id>/<copy_page_pk>/
response:[{
        "id": <id>,
        "title": "<title>",
        "text": "<text>",
        "production_ver": <true/false>,
        "page": "<main_page_id>"
    }]

-PUT pages/<page_id>/<copy_page_pk>/  (only admin)
data: {"title": "<title>",
        "text": "<text>",
        ["production_ver": <true/false>]}
response: [{
        "id": <updated_copy_page_id>,
        "title": "<updated_copy_page_title>",
        "text": "<updated_copy_page_text>",
        "production_ver": <true/false>
    }]

-DELETE pages/<page_id>/<copy_page_pk>/  (only admin)
response: []


-GET pages/<pk>/?production=true
description: get production version of current page
response: [{
        "id": <production_page_id>,
        "title": "<production_page_title>",
        "text": "<production_page_text>",
        "production_ver": true
}]

-GET pages/<pk>/?make_production=true
description: makes current page - production page
response: [{
        "id": <id>,
        "title": "<title>",
        "text": "<text>",
        "production_ver": <true/false>,
        "page": "<main_page_id>"
    }, ...]

- GET pages/<page_id>/<copy_page_pk>/?make_production=true
description: makes current page - production page